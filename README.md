# GoToPills rx-refill

This is to create rx-refill api's in node to get re-fill form and then redirect to refill api page accordingly.   

## How to setup?
* Clone the Repo
* Run `npm install` in root directory
* Then write `npm start` in project terminal.

### Note
* Do setup of `node` and `mongodb` before that.